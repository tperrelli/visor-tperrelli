
## About The Project

This is Visor sample project provided by Tiago Perrelli

## Cloning the project

- Simply run `git clone git@gitlab.com:tperrelli/visor-tperrelli.git`

## Installing

- composer install

## Running all tests

- ./vendor/phpunit/phpunit/phpunit
