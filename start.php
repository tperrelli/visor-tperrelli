<?php

/**
 * Guia prático  EFD Contribuições
 * 
 * @link http://sped.rfb.gov.br/pasta/show/1989
 */

require 'vendor/autoload.php';

use Visor\Services\File;
use Visor\Services\Company;

list(
    $routine, 
    $companyIdentification, 
    $origin, 
    $destination
) = $argv;

$file = new File($origin);

$company = new Company();
$fileNames = $file->scanDir();

foreach ($fileNames as $fileName)
{
    File::checkFile($fileName, $companyIdentification);
    $company->pushContent($file->getFileContent($fileName));
}
File::writeJson($companyIdentification, $company->extractContent(), $destination);
