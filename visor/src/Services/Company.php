<?php

namespace Visor\Services;

use DateTime;
use Visor\Services\File;

class Company
{
    /**
     * 0000 - Company
     * 
     * @var string COMPANY 
     */
    const COMPANY = "0000";

    /**
     * A010 - Company identification number
     * 
     * @var string IDENTIFICATION 
     */
    const IDENTIFICATION = "A010";

    /**
     * A100 - INVOICE identification
     * 
     * @var string INVOICE 
     */
    const INVOICE = "A100";

    /**
     * A010 - INVOICE ITENS
     * 
     * @var string INVOICE_ITENS 
     */
    const INVOICE_ITEMS = "A170";

    /**
     * Order indication
     * 
     * @var string ORDER 
     */
    const ORDER = 0;

    /**
     * Sale indication
     * 
     * @var string INVOICE 
     */
    const SALE = 1;

    /**
     * Current content
     * 
     * @var array $content
     */
    protected $content;

    /**
     * Company 
     * 
     * @var array $company
     */
    protected $company;

    /**
     * Contents
     * 
     * @var array $contents
     */
    protected $contents;

    /**
     * Push content 
     * 
     * @param array $content
     * @return void
     */
    public function pushContent($content)
    {
        $this->contents[] = $content;
    }

    /**
     * Extract content from all buffered contents
     * 
     * @return array
     */
    public function extractContent() : array
    {
        $this->extractCompany();
        foreach ($this->contents as $content)
        {
            $this->content = $content;
            $this->extractCompanyInvoices();
        }

        return $this->export();
    }

    /**
     * Extract company data
     * 
     * @return void
     */
    public function extractCompany()
    {
        $company = self::COMPANY;
        $pattern = "/^\|${company}\|[0-9]{3,3}[0-9A-Z-| ]{1,}/";

        $content = reset($this->contents);
        $matches = File::find($pattern, $content);
        $matches = reset($matches) ?: [];
    
        $this->setCompany($matches);
    }

    /**
     * Set company data
     * 
     * @param string $data
     */
    public function setCompany($data)
    {
        $data = explode("|", $data);

        list(
            $nothing,
            $register, 
            $codeVersion, 
            $type, 
            $situation, 
            $invoiceNumber, 
            $fileDateSate, 
            $fileDateEnd, 
            $name, 
            $identification,
            $state,
            $cityCode
        ) = $data;

        $this->company = [
            'name' => $name,
            'identification' => $identification,
        ];
    }

    /**
     * Retrieves company identification
     * 
     * @return string
     */
    public function getIdentification(): string
    {
        return $this->company['identification'];
    }
    
    /**
     * Extracts all company invoices
     * 
     * @param string $code
     * 
     * A100 - Company invoice
     * A170 - Company invoice itens
     */
    public function extractCompanyInvoices()
    {
        $identification = $this->company['identification'];
        $companyId      = self::IDENTIFICATION;

        $pattern = "/\|${companyId}\|${identification}\|/";
        $content = File::startsFrom($pattern, $this->content);
        
        $invoices = [];
        $companyIdentification = null;

        foreach ($content as $row)
        {
            if (strpos($row, self::IDENTIFICATION) !== false) {
                $companyIdentification = array_filter(explode("|", $row), 'trim');
                $companyIdentification = end($companyIdentification);
                continue;
            }

            $invoice = self::INVOICE;
            
            $invoicePattern = "/\|${invoice}\|[01]\|[0-9F|]{1,}/";
            $matchesInvoice = preg_match($invoicePattern, $row);

            if (File::matches($invoicePattern, $row)) {
                $this->setInvoice($companyIdentification, $row);
            }
        }
    }

    /**
     * Set invoice to company array
     * 
     * @param string $identification
     * @param string $row
     */
    public function setInvoice(string $identification, string $row)
    {
        $invoice = explode("|", $row);
        
        list(
            $nothing,
            $register,
            $operation,
            $emitentIndicator,
            $participantCode,
            $situationCode,
            $serie,
            $subSerie,
            $documentNumber,
            $nfseKey,
            $invoiceDate,
            $invoiceExecDate,
            $totalValue,
            $paymentType,
            $discount,
            $calcBasisValue,
            $calcBasisPis,
            $pisTotalValue,
            $calcBasisConfins,
            $confinsTotalValue,
        ) = $invoice;

        $invoiceDate = DateTime::createFromFormat("dmY", $invoiceDate);

        $invoice = [
            'identification' => $identification,
            'companyName'    => $this->company['name'],
            'totalValue'     => $this->normalizeNumber($totalValue),
            'date'           => $invoiceDate->format('d/m/Y'),
        ];

        if ($operation == self::ORDER) {
            $operationName = 'orders';
        } else if ($operation == self::SALE) {
            $operationName = 'sales';
        }

        $this->company[$operationName][$invoiceDate->format('Y/m')][] = $invoice;    

        $this->setInvoiceItems($operation, $invoiceDate->format('Y/m'), $row);
    }

    /**
     * Set invoice items
     * 
     * @param string $operation (0=salve 1=order)
     * @param string $date 2020/11
     * @param string $string
     */
    public function setInvoiceItems(string $operation, string $date, string $string)
    {        
        $index   = 0;
        $pattern = "/\|A100\|[01|F]{1,}/";
        $content = File::startsFrom($pattern, $this->content);

        if ($string === $content[$index]) $index++;

        do {

            $invoiceItem = explode("|", $content[$index]);

            list(
                $nothing,
                $register,
                $number,
                $code,
                $description,
                $unitValue,
                $discount,
                $baseCodeCredit,
                $originIndicator,
                $tributaryCode,
                $calcBaseValue,
                $aliquote,
                $pisAliquote,
                $pisValue,
            ) = $invoiceItem;

            $unitValue = $this->normalizeNumber($unitValue);
            $discount = $this->normalizeNumber($discount);
    
            $invoiceItem = [
                'number'      => $number,
                'code'        => $code,
                'description' => $description,
                'unitValue'   => ($unitValue - $discount),
                'totalValue'  => $unitValue
            ];

            $invoiceItems[] = $invoiceItem;
            $index++;

        } while(strpos($content[$index], "|" . self::INVOICE_ITEMS) !== false);

        if ($operation == self::ORDER) {
            $operationName = 'orders';
        } else if ($operation == self::SALE) {
            $operationName = 'sales';
        }

        foreach ($this->company[$operationName][$date] as $index => $invoiceRow)
        {
            if (!isset($invoiceRow['items'])) {
                $this->company[$operationName][$date][$index]['items'] = $invoiceItems;
            }
        }        
    }

    /**
     * Export the company data
     * 
     * @return array
     */
    private function export() : array
    {
        $content['unidade'] = [
            'nome'    => $this->company['name'],
            'cnpj'    => $this->company['identification'],
            'compras' => isset($this->company['orders']) ? $this->company['orders'] : [],
            'vendas'  => isset($this->company['sales']) ? $this->company['sales'] : []
        ];

        return $content;
    }

    /**
     * Normalize number
     * 
     * @param string $numbe
     * @return float
     */
    private function normalizeNumber($number) : float
    {
        return (float) number_format(str_replace([',', '.'], '', $number), 2, '.', '');
    }
}