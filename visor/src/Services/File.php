<?php

namespace Visor\Services;

use Visor\Exceptions\InvalidFileException;

class File
{
    /**
     * File origin path
     * 
     * @var string $origin
     */
    private $origin;

    /**
     * @constructor
     * 
     * @param string $origin
     */
    public function __construct(string $origin)
    {
        $this->origin = './' . $origin;
    }

    public static function checkFile(string $fileName, string $identification)
    {
        if (strpos($fileName, $identification) === false) {
            throw new InvalidFileException("Arquivo inválido");
        }

        return true;
    }

    /**
     * Read dir and return all files
     * 
     * @return array
     */
    public function scanDir() : array
    {
        $directory = $this->origin;
        
        return array_values(array_diff(scandir($directory), ['.', '..']));
    }

    /**
     * Read file content
     * 
     * @param string $fileName
     * @param array
     */
    public function getFileContent(string $fileName) : array
    {
        return explode("\n", trim(file_get_contents($this->origin . '/' . $fileName)));
    }

    /**
     * Write file in json format
     * 
     * @param string $fileName
     * @param array $fileName
     * @param string $destination
     */
    public static function writeJson(string $fileName, array $content, string $destination)
    {
        $destination = $destination . '/' . $fileName . '/efd-piscofins/';
        $dirExists = file_exists($destination);
        
        if (!$dirExists) mkdir($destination, 0777, true);

        $file =  $destination . '/' . $fileName . '_compras_vendas.json';
        $content = json_encode($content, JSON_UNESCAPED_UNICODE);

        file_put_contents($file, $content);
    }

    /**
     * Start reading the array from the Regular expression pattern
     * 
     * @param string $pattern
     * @param array $content
     * @return array
     */
    public static function startsFrom(string $pattern, array $content) 
    {
        $startsFrom = 0;
        foreach($content as $key => $row)
        {
            if (preg_match($pattern, $row, $matches)) {
                $startsFrom = $key;
                break;
            }
        }
    
        return array_slice($content, $startsFrom);
    }

    /**
     * Test if a specfig string matches a pattern
     * 
     * @param string $pattern
     * @param string string
     * @return bool
     */
    public static function matches(string $pattern, string $string) 
    {
        return !empty(preg_match($pattern, $string, $matches)) ? true : false;
    }

    /**
     * Find a string by a Regular Expression
     * 
     * @param string $pattern
     * @param array $content
     * @return array
     */
    public static function find(string $pattern, array $content)
    {
        $matches = array_reduce($content, array(File::class, 'matcher'), array($pattern));
        unset($matches[0]);

        return array_values($matches);
    }

    /**
     * Return all the matched strings from pattern test
     * 
     * @param array $match
     * @param string $string
     * @return array
     */
    private static function matcher($match, $string)
    {
        $pattern = $match[0];
    
        if (preg_match($pattern, $string, $matches)) $match[] = $matches[0];

        return $match;
    }
}
