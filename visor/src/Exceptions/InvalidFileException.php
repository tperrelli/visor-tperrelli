<?php

namespace Visor\Exceptions;

use Exception;

class InvalidFileException extends Exception
{}