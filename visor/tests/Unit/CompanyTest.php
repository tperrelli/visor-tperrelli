<?php

namespace Visor\Test\Unit;

use Visor\Services\File;
use Visor\Services\Company;
use PHPUnit\Framework\TestCase;

class CompanyTest extends TestCase
{
    /**
     * Tests if company line is "0000"
     *
     * @return void
     */
    public function test_company_const()
    {
        $company = new Company;

        $this->assertTrue($company::COMPANY === "0000");
    }

    /**
     * Tests if company identification is "A010"
     *
     * @return void
     */
    public function test_company_identification()
    {
        $company = new Company;

        $this->assertTrue($company::IDENTIFICATION === "A010");
    }
    
    /**
     * Tests if company invoice line is "A0100"
     *
     * @return void
     */
    public function test_company_invoice()
    {
        $company = new Company;

        $this->assertTrue($company::INVOICE === "A100");
    }
    
    /**
     * Tests if company invoice is "A0170"
     *
     * @return void
     */
    public function test_company_invoice_items()
    {
        $company = new Company;

        $this->assertTrue($company::INVOICE_ITEMS === "A170");
    }

    public function test_json_string()
    {
        $file = new File('arquivos');

        $company = new Company();
        $id = '99999999000191';

        $fileNames = $file->scanDir();
        foreach ($fileNames as $fileName)
        {
            File::checkFile($fileName, $id);
            $company->pushContent($file->getFileContent($fileName));
        }
        $content = $company->extractContent();

        $file = './destino/99999999000191/efd-piscofins/99999999000191_compras_vendas.json';
        
        $this->assertJsonStringEqualsJsonFile(
            $file, json_encode($content)
        );
    }
}