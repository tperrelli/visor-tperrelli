<?php

namespace Visor\Test\Unit;

use Visor\Services\File;
use PHPUnit\Framework\TestCase;

class LoadFileTest extends TestCase
{
    /**
     * Tests if cnpj/file is valid
     *
     * @return void
     */
    public function test_cnpj_file_is_valid()
    {
        $name = "PISCOFINS_20110101_20110130_99999999000191_Original_20150408094337_573281098CF75BE537CCDEFA0DC763CB1B4D7050.txt";
        $identification = "99999999000191";
        
        $this->assertTrue(File::checkFile($name, $identification));
    }

    /**
     * Tests if cnpj/file is valid
     * 
     * @return void
     */
    public function test_cnpj_file_is_invalid()
    {
        $this->expectException(\Visor\Exceptions\InvalidFileException::class);

        $name = "PISCOFINS_20110101_20110130_99999999000191_Original_20150408094337_573281098CF75BE537CCDEFA0DC763CB1B4D7050.txt";
        $identification = "99999999000190";

        File::checkFile($name, $identification);
    }    
}